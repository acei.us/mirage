# mirage
The Nerd Horde moderation solution, by Aceius. Ported to discord.js by JewelEyed.  
This software is licenced under a modified JSON licence:
- It may not be used to train Machine Learning models

## Documentation
Setting up mirage is really simple:
```sh
git clone https://gitlab.com/acei.us/mirage
cd mirage
npm install
touch config.json
```
Place some content in the config file:
```js
{
    "token": "your token",
    "clientId": "your appid",
    "profilePicture": "Some image",
    "version": "here for backwards compatibility reasons, set to -69420 for all I care"
}
```
```sh
node bot.js
```
