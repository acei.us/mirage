const { Client, Intents } = require('discord.js');
const { token, profilePicture, version, clientId } = require('./config.json');
global.fs = require('fs');
global.path = require('path');

global.id = clientId;
global.version = version;
global.profilePicture = profilePicture;

// An formatted date
global.dateFormatted = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');

global.cwd = __dirname;
require('./process/console');
require('./process/backup');
require('./process/events');

/*
try {
    fs.mkdirSync('./backups').catch()
    fs.copyFile('./log.txt')
}*/

global.client = new Client({
	intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_MESSAGE_REACTIONS, Intents.FLAGS.GUILD_VOICE_STATES],
	partials: ['MESSAGE', 'CHANNEL', 'REACTION'],
});

require('./loaders/events');
require('./loaders/commands');
client.login(token);
require('./process/exit');

