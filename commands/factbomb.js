const facts = [
	'The factbomb command is not defective. Its facts are wholly accurate and very interesting.',
	'Dental floss has superb tensile strength.',
	'Avocados have the highest fiber and calories of any fruit. They are found in Australians.',
	'Hot water freezes quicker than cold water.',
	'Roman toothpaste was made with human urine. Urine as an ingredient in toothpaste continued to be used up until the 18th century.',
	'Aceius is very cool.',
	'I am not christian.',
	'Contrary to popular belief, the Eskimo does not have one hundred different words for snow. They do, however, have two hundred and thirty-four words for fudge.',
	'The first person to prove that cow\'s milk is drinkable was very, very thirsty.',
	'At some point in their lives 1 in 6 children will be abducted by the Dutch.',
	'To make a photocopier, simply photocopy a mirror.',
	'According to most advanced algorithms, the world\'s best name is Austin.',
	'If you have trouble with simple counting, use the following mnemonic device: one comes before two comes before 60 comes after 12 comes before six trillion comes after 504. This will make your earlier counting difficulties seem like no big deal.',
];

const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('factbomb')
		.setDescription('Sends a random fact.'),

	async execute(interaction) {
		content = facts[Math.floor(Math.random() * facts.length)];
		const embed = new MessageEmbed()
			.setTitle('Fact')
			.setColor('#1d1e35')
			.setDescription(content)
			.setFooter(
				{ text: 'This is absolute truth' },
			);

		try {
			return await interaction.reply({ embeds: [embed] });
		}
		catch (er) {
			try {
				console.error(er);
				return await interaction.followUp({ content:'Something went wrong!', ephemeral: true });
			}
			catch (errr) {
				console.error(er);
				return await interaction.reply({ content:'Something went wrong!', ephemeral: true });
			}
		}
	},
};