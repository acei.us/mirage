const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('help')
		.setDescription('Helpful with the bot.'),

	async execute(interaction) {
		const embed = new MessageEmbed()
			.setTitle('Help')
			.setColor('#1d1e35')
			.addFields(
				{
					name: 'Commands',
					value: `Type \`/[command]\` to trigger a command, or use the + button on the message bar.
                    \`/help\` - Get help with Mirage.
                    \`/ping\` - Replies with Pong!
                    \`/info\` - About me.
                    \`/members\` - Returns the member count.`,
				},
				{
					name: 'Fun',
					value: `\`/pandabomb\` - Sends a random panda GIF.
                    \`/factbomb\` - Sends a random 'fact'.
                    \`/hexcode\` - Displays an image of a HEX value.`,
				},

			)
			.setFooter(
				{ text: 'Contact Aceius at https://acei.us/contact' },
			);


		try {
			return await interaction.reply({ embeds: [embed] });
		}
		catch (er) {
			try {
				console.error(er);
				return await interaction.followUp({ content:'Something went wrong!', ephemeral: true });
			}
			catch (errr) {
				console.error(er);
				return await interaction.reply({ content:'Something went wrong!', ephemeral: true });
			}
		}
	},
};