const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('hexcode')
		.setDescription('Display a image of a hexcode.')
		.addStringOption(option =>
			option
				.setName('color')
				.setRequired(true)
				.setDescription('The hexcode to display.')),

	async execute(interaction) {
		try {
			inp = interaction.options.getString('color');
			return await interaction.reply({ content: /^#[0-9A-F]{6}$/i.test('#' + inp) ? content = `https://singlecolorimage.com/get/${inp}/256x256` : content = 'That is not a valid hex code.' });
		}
		catch (er) {
			try {
				console.error(er);
				return await interaction.followUp({ content:'Something went wrong!', ephemeral: true });
			}
			catch (errr) {
				console.error(er);
				return await interaction.reply({ content:'Something went wrong!', ephemeral: true });
			}
		}
	},
};