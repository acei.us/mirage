const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('info')
		.setDescription('Information about the bot.'),

	async execute(interaction) {
		const embed = new MessageEmbed()
			.setTitle('About')
			.setColor('#1d1e35')
			.setDescription(`Mirage version \`${version}\``)
			.addFields(
				{
					name: 'About',
					value: 'Mirage is the Nerd Horde moderation solution, made by Aceius (https://acei.us/).\nWant me in your server? Contact Aceius.',
				},
				{
					name: 'Testimonials',
					value: '"`b? pandabomb` is the best command" - ahsokasvoid\n"is botceius cristian?" - ILoveArsonAndTaxFraud\n"oh god not this bot" - Josukeihei',
				},

			);


		try {
			return await interaction.reply({ embeds: [embed] });
		}
		catch (er) {
			try {
				console.error(er);
				return await interaction.followUp({ content:'Something went wrong!', ephemeral: true });
			}
			catch (errr) {
				console.error(er);
				return await interaction.reply({ content:'Something went wrong!', ephemeral: true });
			}
		}
	},
};