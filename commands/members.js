const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('members')
		.setDescription('The member list of this guild.'),

	async execute(interaction) {
		if (interaction.guild == undefined) {
			return await interaction.reply({ content: 'This isn\'t a guild...' });
		}
		const memberCount = interaction.guild.memberCount;
		const embed = new MessageEmbed()
			.setColor('#1d1e35')
			.setDescription(`This guild has ${memberCount} total members.`);


		try {
			return await interaction.reply({ embeds: [embed] });
		}
		catch (er) {
			try {
				console.error(er);
				return await interaction.followUp({ content:'Something went wrong!', ephemeral: true });
			}
			catch (errr) {
				console.error(er);
				return await interaction.reply({ content:'Something went wrong!', ephemeral: true });
			}
		}
	},
};