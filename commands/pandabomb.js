const gifs = [
	'https://imgur.com/ZawJpDb',
	'https://tenor.com/view/saiai-sai-gif-23790844',
	'https://tenor.com/view/panda-gif-5303855',
	'https://tenor.com/view/panda-door-hide-cub-gif-7957807',
	'https://tenor.com/view/happy-panda-bamboo-panda-hungry-panda-eating-gif-17804693',
	'https://tenor.com/view/panda-cute-tongue-out-licking-gif-15717391',
	'https://images-ext-1.discordapp.net/external/VUsbabkkhksrRBpZ5vvWAD17IFKSnZdxVXvuEPsFeOc/https/media.tenor.com/F1bqQ93ZfO0AAAPo/panda-revenge.mp4',
	'https://media.tenor.com/EWPjzf41UAgAAAAS/gfg.gif',
	'https://media.tenor.com/PezPHRkzuhUAAAAd/panda-seulisasoo.gif',
];
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('pandabomb')
		.setDescription('Sends a random panda gif.'),

	async execute(interaction) {
		content = gifs[Math.floor(Math.random() * gifs.length)];
		try {
			const message = await interaction.reply({ content: content, fetchReply: true });
			await message.react('🐼');
			return await message.react('💣');
		}
		catch (er) {
			try {
				console.error(er);
				return await interaction.followUp({ content:'Something went wrong!', ephemeral: true });
			}
			catch (errr) {
				console.error(er);
				return await interaction.reply({ content:'Something went wrong!', ephemeral: true });
			}
		}
	},
};