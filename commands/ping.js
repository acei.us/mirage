const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('ping')
		.setDescription('Replies with Pong!'),

	async execute(interaction) {
		const embed = new MessageEmbed()
			.setTitle('Pong!')
			.setColor('#2f3136');

		try {
			return await interaction.reply({ embeds: [embed], ephemeral: true });
		}
		catch (er) {
			try {
				console.error(er);
				return await interaction.followUp({ content:'Something went wrong!', ephemeral: true });
			}
			catch (errr) {
				console.error(er);
				return await interaction.reply({ content:'Something went wrong!', ephemeral: true });
			}
		}
	},
};