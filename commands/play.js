const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageEmbed } = require('discord.js');
const { joinVoiceChannel, createAudioPlayer, createAudioResource, StreamType } = require('@discordjs/voice');
const ytdl = require('ytdl-core');
/*
* This is a example of how you would make a play system so uh yay :)
module.exports = {
	data: new SlashCommandBuilder()
		.setName('play')
		.setDescription('Play something in a voice channel.')
		.addStringOption(option =>
			option.setName('youtube_url')
				.setDescription('YouTube URL')
				.setRequired(true)),

	async execute(interaction) {
		if (!interaction.guild) {
			return await interaction.reply({ content: 'This isn\'t a guild...' });
		}

		const embed = new MessageEmbed()
			.setColor('#1d1e35')
			.setDescription('Starting playback...');

		try {
			await interaction.reply({ embeds: [embed] });

			const voiceChannel = interaction.member.voice.channel;
			if (!voiceChannel) {
				return await interaction.followUp({ content: 'You must be in a voice channel to use this command.', ephemeral: true });
			}

			const connection = voiceChannel.guild.voiceConnection;

			if (connection) {
				// Already connected to a voice channel
				return await interaction.followUp({ content: 'Already connected to a voice channel.', ephemeral: true });
			}

			const youtubeUrl = interaction.options.getString('youtube_url');
			const stream = ytdl(youtubeUrl, { filter: 'audioonly' });

			const player = createAudioPlayer();
			const resource = createAudioResource(stream, { inputType: StreamType.Opus });

			connection.subscribe(player);
			player.play(resource);

			await interaction.followUp({ content: 'Successfully joined the voice channel and playing the audio!' });
		}
		catch (error) {
			console.error(error);
			await interaction.followUp({ content: 'Something went wrong!', ephemeral: true });
		}
	},
};

*/