const gifs = [
	'https://tenor.com/view/engineer-engineer_gaming-gaming-bruhmoment-tf2-gif-19011890',
	'https://tenor.com/view/tf2-team-fortress2-engineer-engineer-tf2-eltorro64rus-gif-24514057',
	'https://tenor.com/view/tf2-ceno0-heavy-gif-20401109',
	'https://tenor.com/view/dead-heavy-gif-19753303',
	'https://tenor.com/view/demoman-shooting-memes-shooting-guns-tf2-gif-18730188',
	'https://tenor.com/view/tf2-scout-team-fortress2-valve-source-gif-25519673',
];
const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('tf2bomb')
		.setDescription('Sends a random tf2 gif.'),

	async execute(interaction) {
		content = gifs[Math.floor(Math.random() * gifs.length)];
		try {
			const message = await interaction.reply({ content: content, fetchReply: true });
			await message.react('<:kerbal:1031007907887726652>');
		}
		catch (er) {
			try {
				console.error(er);
				return await interaction.followUp({ content:'Something went wrong!', ephemeral: true });
			}
			catch (errr) {
				console.error(er);
				return await interaction.reply({ content:'Something went wrong!', ephemeral: true });
			}
		}
	},
};