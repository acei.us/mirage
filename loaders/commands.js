const fs = require('fs');
const path = require('path');
const { Collection } = require('discord.js');

global.client.commands = new Collection();

const commandsPath = path.join(cwd, 'commands');

function readCommandsFromDirectory(directory) {
	const files = fs.readdirSync(directory);
	for (const file of files) {
		const filePath = path.join(directory, file);
		const stat = fs.statSync(filePath);
		if (stat.isFile()) {
			const command = require(filePath);
			client.commands.set(command.data.name, command);
		}
		else if (stat.isDirectory()) {
			readCommandsFromDirectory(filePath);
		}
	}
}
readCommandsFromDirectory(commandsPath);
require('./deploy-commands');
require('./buttons');
