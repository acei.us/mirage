const fs = require('node:fs');
const path = require('node:path');
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');
const { clientId, token } = require('../config.json');

const commands = [];
const commandsPath = path.join(cwd, 'commands');

function readCommandsFromDirectory(directory) {
	const files = fs.readdirSync(directory);
	for (const file of files) {
		const filePath = path.join(directory, file);
		const stat = fs.statSync(filePath);
		if (stat.isFile()) {
			const command = require(filePath);
			commands.push(command.data.toJSON());
		}
		else if (stat.isDirectory()) {
			readCommandsFromDirectory(filePath);
		}
	}
}
readCommandsFromDirectory(commandsPath);

const rest = new REST({ version: '9' }).setToken(token);

rest.put(Routes.applicationCommands(clientId), { body: commands })
	.then(() => console.log('Successfully registered application commands.'))
	.catch(console.error);