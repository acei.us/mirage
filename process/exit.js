exitQ = async function() {
	const prompt = require('async-prompt');
	const q = await prompt('');
	if (q.toLowerCase() === 'exit') {
		process.exit(0);
	}
	else {
		exitQ();
	}
};
exitQ();